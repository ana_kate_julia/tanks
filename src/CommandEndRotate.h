#pragma once

#include <iostream>
#include <vector>
#include "ICommand.h"
#include "IUObject.h"
#include "EmptyCmd.h"

 
class CommandEndRotate : public ICommand {
private:
	IUObject& _obj;
	EmptyCommand _emptyCommand;
public:
	CommandEndRotate(IUObject& obj) : _obj(obj) {}

	void execute() {
		_obj.setProperty("rotateCommand", static_cast<ICommand*>(&_emptyCommand));
	}
	

	~CommandEndRotate() = default;
};
