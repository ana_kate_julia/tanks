#pragma once

#include <iostream>
#include <vector>
#include "ICommand.h"
#include "IUObject.h"
#include "IRotable.h"
 

template <typename T>
class CommandRotate : public ICommand {
private:
	IRotable<T>& _rotateAdapter;
 
public:
	CommandRotate(IRotable<T>& rotateAdapter) : _rotateAdapter(rotateAdapter) {}

	void execute() {
        std::vector<T>&& angle = _rotateAdapter.getAngle();
        const std::vector<T>&& angleVelocity = _rotateAdapter.getAngleVelocity();

        const int dimOfSpace = std::min(angle.size(), angleVelocity.size());

        for (int i = 0; i < dimOfSpace; i++)
        {
            angle[i] = (angle[i] + angleVelocity[i]) % 360;
        }
        _rotateAdapter.setAngle(angle);
	}

	~CommandRotate() = default;
};
