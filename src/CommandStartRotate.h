#pragma once

#include <iostream>
#include <vector>
#include "ICommand.h"
#include "IUObject.h"
#include "IRotable.h"
#include "CommandRotate.h"

template <typename T>
class CommandStartRotate : public ICommand {
private:
	IUObject& _obj;
	std::vector<T> _angleVelocity;
	IRotable<T>& _rotateAdapter;
	std::vector<T> _zerosVector;
public:
	CommandStartRotate(IUObject& object, IRotable<T>& rotateAdapter, std::vector<T>& angleVelocity) :
		_obj(object), _rotateAdapter(rotateAdapter), _angleVelocity(angleVelocity)
	{
		_zerosVector = std::vector<T>(angleVelocity.size(), 0);

		ICommand* rotateCommandPtr = this;
		_obj.setProperty("rotateCommand", rotateCommandPtr);
	}

	void execute() {

		_rotateAdapter.setAngleVelocity(_angleVelocity);
		CommandRotate<T> rotateCommand(_rotateAdapter);
		rotateCommand.execute();
		_rotateAdapter.setAngleVelocity(_zerosVector);
	}

	~CommandStartRotate() = default;
};
