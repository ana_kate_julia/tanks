#pragma once

#include <exception>
#include <vector>
#include <any>

class ICommand {
public:
	virtual void execute() = 0;
	virtual ~ICommand() = default;
};

class CommandException : public std::exception {
private:
	std::vector<std::any>* _args;

public:
	CommandException(std::vector<std::any> args) : _args(&args) {

	}

	CommandException() {
		_args = new std::vector<std::any>;
	}

	void addArg(std::any arg) {
		_args->push_back(arg);
	}

	void setArgs(std::vector<std::any> args) {
		_args = &args;
	}

	std::vector<std::any> getArgs() {
		return *_args;
	}
};
