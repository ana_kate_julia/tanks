#pragma once

#include <vector>

template <typename T>
class IRotable 
{
public:
	virtual std::vector <T> getAngle() = 0;
	virtual void setAngle(std::vector <T> newValue) = 0;
	virtual std::vector <T> getAngleVelocity() = 0;
	virtual void setAngleVelocity(std::vector<T> newValue) = 0;
};
