#pragma once

#include "ICommand.h"
#include "CommandMove.h"
#include <exception>
#include <vector>
#include <any>
#include "IoC/IoC.h"


class CommandBackupMovable : public ICommand {
private:
	IMovable<int>& _moveAdapt;
	std::vector<int> _pos;
	std::vector<int> _vel;
public:
	CommandBackupMovable(IMovable<int>& moveAdapt) :
		_moveAdapt(moveAdapt)
	{
		_pos = _moveAdapt.getPosition();
		_vel = _moveAdapt.getVelocity();
	}

	void execute() {
		_moveAdapt.setPosition(_pos);
		_moveAdapt.setVelocity(_vel);
	}
};


class MacroCommandMove : public ICommand {
private:
	IMovable<int>& _moveAdapt;
public:
	MacroCommandMove(IMovable<int>& moveAdapt) : 
		_moveAdapt(moveAdapt){
	}

	void execute() {
		CommandBackupMovable backupCmd(_moveAdapt);
		try {
			CommandMove<int> cmd(_moveAdapt);
			cmd.execute();
		}
		catch(CommandException e){
			e.addArg(backupCmd);
			throw e;
		}
	}
};
