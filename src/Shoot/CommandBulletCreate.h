#pragma once

#include "ICommand.h"
#include "IUObject.h"
#include "Shoot/IBullet.h"
#include <vector>

template <typename T>
class CommandBulletCreate : public ICommand {
private:
	IUObject& _obj;
	std::vector<T>& _position;
	std::vector<T>& _velocity;
	std::vector<T>& _angle;

public:
	CommandBulletCreate(IUObject& obj, std::vector<T>& position, std::vector<T>& velocity, std::vector<T>& angle) :
		_obj(obj), _position(position), _velocity(velocity), _angle(angle) {}

	void execute()
	{
		_obj.setProperty("Position", _position);
		_obj.setProperty("Velocity", _velocity);
		_obj.setProperty("Angle", _angle);
	}
};
