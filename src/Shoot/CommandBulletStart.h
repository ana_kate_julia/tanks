#pragma once

#include "ICommand.h"
#include "IUObject.h"
#include <vector>

template <typename T>
class CommandBulletStart : public ICommand {
private:
	IUObject& _obj;

public:
	CommandBulletStart(IUObject& obj) : _obj(obj) {}

	void execute()
	{
		_obj.setProperty("moveCommand", this);
		
		std::vector<T> position = std::any_cast<std::vector<T>>(_obj.getProperty("Position"));
		std::vector<T> velocity = std::any_cast<std::vector<T>>(_obj.getProperty("Velocity"));

		const int dim = std::min(position.size(), velocity.size());
		
		for (int i = 0; i < dim; i++) {
			position[i] += velocity[i];
		}

		_obj.setProperty("Position", position);
	}
};
