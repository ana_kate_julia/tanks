#pragma once

#include "ICommand.h"
#include "IUObject.h"
#include "Shoot/IBullet.h"
#include <vector>

template <typename T>
class CommandMacroBullet : public ICommand {
private:
	CommandBulletCreate<T>& _cmdCreate;
	CommandBulletStart<T>& _cmdStart;

public:
	CommandMacroBullet(CommandBulletCreate<T>& cmdCreate, CommandBulletStart<T>& cmdStart) :
		_cmdCreate(cmdCreate), _cmdStart(cmdStart){}

	void execute()
	{
		try {
			_cmdCreate.execute();
			_cmdStart.execute();
		}
		catch (...) {
			throw CommandException();
		}
	}
};
