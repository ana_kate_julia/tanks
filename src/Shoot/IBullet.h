#pragma once

template <typename T>
class IBullet {
public:
	virtual std::vector<T> getPosition() = 0;
	virtual void setPosition(std::vector<T> value) = 0;
	virtual std::vector<T> getVelocity() = 0;
	virtual void setVelocity(std::vector<T> value) = 0;
	virtual std::vector<T> getAngle() = 0;
	virtual void setAngle(std::vector<T> value) = 0;
	virtual ~IBullet() = default;
};
