#include "UObject.h"

std::any UObject::getProperty(const std::string& key) {
	return _properties.at(key); //exception if not exist
}

void UObject::setProperty(const std::string& key, const std::any& newValue) {
	_properties[key] = newValue;
}
