#pragma once

#include <iostream>
#include <any>
#include <map>
#include "IUObject.h"

class UObject : public IUObject {
private:
	std::map<std::string, std::any> _properties;
public:
	std::any getProperty(const std::string& key);
	void setProperty(const std::string& key, const std::any& newValue);
	~UObject() = default;
};
