#include "gmock/gmock.h"  // Brings in gMock.
#include <vector>
#include "IRotable.h"
#include "IVelocity.h"

 

template <typename T>
class MockRotable : public IVelocity<T>, public IRotable<T>
{
private:
	std::vector<T> _angle;
	std::vector<T> _angleVelocity;
	std::vector<T> _velocity;
public:
	MOCK_METHOD(std::vector<T>, getAngle, (), (override));
	MOCK_METHOD(void, setAngle, (std::vector<T> value), (override));
	MOCK_METHOD(std::vector<T>, getAngleVelocity, (), (override));
	MOCK_METHOD(void, setAngleVelocity, (std::vector<T> value), (override));
	MOCK_METHOD(void, setVel, (std::vector<T> value), (override));
	MOCK_METHOD(std::vector<T>, getVel, (), (override));
	

	MockRotable()
	{
		ON_CALL(*this, getAngle).WillByDefault([this]()
		{
			return this->_angle;
		});

		ON_CALL(*this, setAngle).WillByDefault([this](std::vector<T> newValue)
		{
			this->_angle = newValue;
		});

		ON_CALL(*this, getAngleVelocity).WillByDefault([this]()
		{
			return this->_angleVelocity;
		});

		ON_CALL(*this, setAngleVelocity).WillByDefault([this](std::vector<T> newValue)
		{
			this->_angleVelocity = newValue;
		});
	
		ON_CALL(*this, setVel).WillByDefault([this](std::vector<T> newValue)
		{
			this->_velocity = newValue;
		});
		
		ON_CALL(*this, getVel).WillByDefault([this]()
		{
			return this->_velocity;
		});

	
	}
};

  