#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "UObject.h"
#include "CommandStartRotate.h"
#include "CommandRotate.h"
#include "CommandEndRotate.h"
#include "CommandVelocityCorrect.h"
#include "MockRotable.h"



TEST(ObjRotate, CommandRotate)
{
	std::vector<int> angle{ 42, 10, 13 };
	const std::vector<int> angleVelocity{ 25, 55, 19 };

	MockRotable<int> mockObjRotate;

	EXPECT_CALL(mockObjRotate, setAngle).Times(2);  
	EXPECT_CALL(mockObjRotate, setAngleVelocity).Times(1);  
	EXPECT_CALL(mockObjRotate, getAngle).Times(2);
	EXPECT_CALL(mockObjRotate, getAngleVelocity).Times(1);

	mockObjRotate.setAngle(angle);
	mockObjRotate.setAngleVelocity(angleVelocity);

	CommandRotate<int> rotateCommand(mockObjRotate);
	rotateCommand.execute();

 
	for (int i = 0; i < angle.size(); i++)
	{
		angle[i] = (angle[i] + angleVelocity[i]) % 360;
	}
	EXPECT_EQ(mockObjRotate.getAngle(), angle);
}

TEST(StartObjRotate, CommandStartRotate)
{
	const std::vector<int> angleVelocity{ 25, 55, 19 };
	std::vector<int> zerosVector;
	zerosVector = std::vector<int>(angleVelocity.size(), 0);

	MockRotable<int> mockObjRotate;

	EXPECT_CALL(mockObjRotate, setAngleVelocity).Times(2); 

	mockObjRotate.setAngleVelocity(angleVelocity);

	CommandRotate<int> rotateCommand(mockObjRotate);

	rotateCommand.execute();

	mockObjRotate.setAngleVelocity(zerosVector);

	EXPECT_EQ(mockObjRotate.getAngleVelocity(), zerosVector);
}


TEST(EndObjRotate, CommandEndRotate)
{
	MockRotable<int> mockObjRotate;
	UObject UniversalObjRotate;

	EXPECT_CALL(mockObjRotate, setAngle).Times(0);
	EXPECT_CALL(mockObjRotate, setAngleVelocity).Times(0);
	EXPECT_CALL(mockObjRotate, getAngle).Times(0);
	EXPECT_CALL(mockObjRotate, getAngleVelocity).Times(0);


	CommandEndRotate rotateCommand(UniversalObjRotate);

	rotateCommand.execute();

}



TEST(VelocityCorrect, CommandVelocityCorrect)
{
	MockRotable<int> mockObjRotate;

	std::vector<int> angle{ 28, 15, 19 };
	std::vector<int> velocity{ 12, 45, 29 };
	std::vector<int> velocityCorrectable;
	int radius = 0;
 
	EXPECT_CALL(mockObjRotate, setAngle).Times(2);
	EXPECT_CALL(mockObjRotate, setAngleVelocity).Times(0);
	EXPECT_CALL(mockObjRotate, getAngle).Times(3);
	EXPECT_CALL(mockObjRotate, getAngleVelocity).Times(0);
	EXPECT_CALL(mockObjRotate, getVel).Times(1);
	EXPECT_CALL(mockObjRotate, setVel).Times(2);

	 
 
	mockObjRotate.setAngle(angle);
	mockObjRotate.setVel(velocity);
	

	for (int var : mockObjRotate.getVel())
	{
		radius += var * var;
	}

	velocityCorrectable.push_back((int)(cos(mockObjRotate.getAngle().at(0)) * sqrt(radius)));
	velocityCorrectable.push_back((int)(sin(mockObjRotate.getAngle().at(0)) * sqrt(radius)));
	mockObjRotate.setVel(velocityCorrectable);
	mockObjRotate.setAngle(angle);

	EXPECT_EQ(mockObjRotate.getAngle(), angle);


}
 