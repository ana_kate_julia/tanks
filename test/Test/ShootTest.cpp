#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "UObject.h"

#include "Shoot/CommandBulletCreate.h"
#include "Shoot/CommandBulletStart.h"
#include "Shoot/CommandMacroBullet.h"

TEST(Tanks_ShootComponent, CreateBullet)
{
	std::vector<int> position{ 3, 6 };
	std::vector<int> velocity{ 2, 3 };
	std::vector<int> angle{ 45 };

	UObject obj;

	CommandBulletCreate<int> cmdCreate(obj, position, velocity, angle);
	cmdCreate.execute();

	EXPECT_EQ(std::any_cast<std::vector<int>>(obj.getProperty("Position")), position);
	EXPECT_EQ(std::any_cast<std::vector<int>>(obj.getProperty("Velocity")), velocity);
	EXPECT_EQ(std::any_cast<std::vector<int>>(obj.getProperty("Angle")), angle);
}

TEST(Tanks_ShootComponent, StartBullet)
{
	std::vector<int> position{ 3, 6 };
	std::vector<int> velocity{ 2, 3 };
	std::vector<int> angle{ 45 };

	UObject obj;

	CommandBulletCreate<int> cmdCreate(obj, position, velocity, angle);
	cmdCreate.execute();

	CommandBulletStart<int> cmdStart(obj);
	cmdStart.execute();

	std::vector<int> nextPosition;
	for (int i = 0; i < position.size(); i++) {
		nextPosition.push_back(position[i] + velocity[i]);
	}

	EXPECT_EQ(std::any_cast<std::vector<int>>(obj.getProperty("Position")), nextPosition);
}

TEST(Tanks_ShootComponent, MacroCommand)
{
	std::vector<int> position{ 3, 6 };
	std::vector<int> velocity{ 2, 3 };
	std::vector<int> angle{ 45 };

	UObject obj;

	CommandBulletCreate<int> cmdCreate(obj, position, velocity, angle);
	CommandBulletStart<int> cmdStart(obj);
	
	CommandMacroBullet<int> cmdMacro(cmdCreate, cmdStart);
	cmdMacro.execute();

	std::vector<int> nextPosition;
	for (int i = 0; i < position.size(); i++) {
		nextPosition.push_back(position[i] + velocity[i]);
	}

	EXPECT_EQ(std::any_cast<std::vector<int>>(obj.getProperty("Position")), nextPosition);
}
