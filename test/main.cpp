#include <iostream>
#include "Test/MockMovable.h"
#include "Test/vectorOps.h"
#include "Test/MockRotable.h"
#include "Test/vectorOps.h"

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
